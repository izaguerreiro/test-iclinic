# Teste para a vaga de Desenvolvedor Back-end Python

Solução para o [teste da iClinic](https://github.com/iclinic/iclinic-python-challenge)

### Como executar?

Clone o projeto
```bash
$ git clone git@gitlab.com:izaguerreiro/test-iclinic.git
```

Acesse a pasta do projeto
```bash
$ cd test-iclinic
```

Crie e ative o virtualenv
```bash
$ python -m venv .venv
$ source .venv/bin/activate 
```

Instale os requirements
```bash
$ pip install -r requirements.txt
```

Entre na pasta do projeto
```bash
$ cd app
```

Crie as variáveis de ambiente
```bash
$ export FLASK_APP=api.py
```

Execute os testes
```bash
$ py.test
```

Execute a aplicação
```bash
$ flask run
```

### Como consumir a api?
Retorna os pacientes que iniciam com **Mar**
```bash
$ curl -X GET -H "Content-Type: application/json" http://localhost:5000/autocomplete?q=Mar
```
