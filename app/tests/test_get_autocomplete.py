import unittest
from app.api import app


class TestGetAutocomplete(unittest.TestCase):
    """ Testa função autocomplete """

    def setUp(self):
        a = app.test_client()
        self.response = a.get('/autocomplete?q=Mar')

    def test_status_code(self):
        """ Verifico o status da requisição """
        self.assertEqual(200, self.response.status_code)

    def test_content_type(self):
        """ Verifico o content_type retornado """
        self.assertEqual('application/json', self.response.content_type)
