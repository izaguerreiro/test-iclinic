import unittest
from app.patient import Patient


class TestImportCsv(unittest.TestCase):
    """ Testa o método import_csv da classe Patient """

    def setUp(self):
        self.patients = Patient().import_csv()

    def test_return(self):
        """ Verifico se o retorno do método é uma lista """
        self.assertEqual(type(self.patients), list)