from flask import Flask
from flask import jsonify
from flask import request
from app.patient import Patient


app = Flask('app')

        
@app.route('/autocomplete')
def get_autocomplete():
    """ Retorna um json com os nomes dos pacientes """
    prefix = request.args.get('q')
    patients = Patient().search(prefix.lower())
    return jsonify({'patients': patients}), 200