import csv
from app.tree import Tree


class Patient:

    def __init__(self):
        self.patients = []

    def import_csv(self):
        """ Importa os dados do dataset e guarda em uma lista """
        with open('dataset/patients.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter='\n')
            for c in csv_reader:
                self.patients.append(c[0])
        
        return self.patients

    def search(self, word):
        """ Método para busca de pacientes """
        tree = Tree()
        for patient in self.import_csv():
            tree.insert(patient)
        return list(tree.words_with_prefix(word))