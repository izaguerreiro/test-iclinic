class TreeNode:
    """ Nós da árvore binária """

    def __init__(self):
        self.end = False
        self.children = {}

    def all_words(self, prefix):
        if self.end:
            yield prefix
        
        for letter, child in self.children.items():
            yield from child.all_words(prefix + letter)


class Tree:
    """ Árvore binária """

    def __init__(self):
        self.root = TreeNode()

    def insert(self, word):
        current = self.root
        for letter in word:
            node = current.children.get(letter)
            if not node:
                node = TreeNode()
                current.children[letter] = node
            current = node
        current.end = True

    def search(self, word):
        current = self.root
        for letter in word:
            node = current.children.get(letter)
            if not node:
                return False
            current = node
        return current.end

    def words_with_prefix(self, prefix):
        current = self.root
        
        for p in prefix:
            current = current.children.get(p)
            if current is None:
                return
        
        yield from current.all_words(prefix)